// CS 61C Fall 2015 Project 4

// include SSE intrinsics
#if defined(_MSC_VER)
#include <intrin.h>
#elif defined(__GNUC__) && (defined(__x86_64__) || defined(__i386__))
#include <x86intrin.h>
#endif

// include OpenMP
#if !defined(_MSC_VER)
#include <pthread.h>
#endif
#include <omp.h>

#include <math.h>
#include <stdbool.h>
#include <stdio.h>

#include "calcDepthOptimized.h"
#include "calcDepthNaive.h"

/* DO NOT CHANGE ANYTHING ABOVE THIS LINE. */
#define ABS(x) (((x) < 0) ? (-(x)) : (x))
#define MAX(a, b) ((a > b) ? a : b)
#define MIN(a, b) ((a < b) ? a : b)

float displacement(int dx, int dy)
{
	float squaredDisplacement = dx * dx + dy * dy;
	float displacement = sqrt(squaredDisplacement);
	return displacement;
}


void calcDepthOptimized(float *depth, float *left, float *right, int imageWidth, int imageHeight, int featureWidth, int featureHeight, int maximumDisplacement)
{
	/*Memset makes memory for the vectors. */
	memset(depth, 0, imageHeight*imageWidth*sizeof(float)); 

	/*Trying to do less arithmetic by storing for loop arguments here.*/
	int unrollbyFour = (2 * featureWidth + 1)/4*4;
	int tailCase = (2 * featureWidth + 1);
	

	/*Collapse helps parallizing nested for loops. */
	#pragma omp parallel for collapse(2)
		/* The two outer for loops iterate through each pixel in the feature */
	    for (int y = featureHeight; y < imageHeight-featureHeight; y++) 
	    {
	        for (int x = featureWidth; x < imageWidth-featureWidth; x++)  
	        {

				float minimumSquaredDifference = -1;
				int minimumDy = 0;
				int minimumDx = 0;

				/* Iterate through all feature boxes that fit inside the maximum displacement box. 
				   centered around the current pixel, skipping feature boxes that don't fit. */
				for (int dy = MAX(-maximumDisplacement, featureHeight - y); dy <= MIN(maximumDisplacement, imageHeight - featureHeight - y - 1); dy++)
				{
					for (int dx = MAX(-maximumDisplacement, featureWidth - x); dx <= MIN(maximumDisplacement, imageWidth - featureWidth - x - 1); dx++)
					{

						float squaredDifference = 0;
						float difference[4];
						__m128 sum = _mm_setzero_ps();

						/* UNROLL and find the squared difference.*/
						for (int boxX = 0; boxX < unrollbyFour; boxX+=4)
						{
							for (int boxY = -featureHeight; boxY <= featureHeight; boxY++)
							{

								__m128 leftDiff = _mm_loadu_ps(left + (y + boxY) * imageWidth + x + boxX - featureWidth);
								__m128 rightDiff = _mm_loadu_ps(right + (y + dy + boxY) * imageWidth + x + dx + boxX - featureWidth);
								__m128 subtractDiff = _mm_sub_ps(leftDiff, rightDiff);
								__m128 multDiff = _mm_mul_ps(subtractDiff, subtractDiff);
								sum = _mm_add_ps(sum, multDiff);
							}
						}
						
						_mm_storeu_ps(difference, sum);
						squaredDifference += difference[0] + difference[1] + difference[2] + difference[3];
							
						for (int boxX = unrollbyFour; boxX < tailCase; boxX++)
						{
							for (int boxY = -featureHeight; boxY <= featureHeight; boxY++)
							{
								int leftX = x + boxX - featureWidth;
								int leftY = y + boxY;
								int rightX = x + dx + boxX - featureWidth;
								int rightY = y + dy + boxY;

								float tailDiff = left[leftY * imageWidth + leftX] - right[rightY * imageWidth + rightX];
								squaredDifference += tailDiff * tailDiff;
							}
						}
			

						/* 
						Check if you need to update minimum square difference. 
						This is when either it has not been set yet, the current
						squared displacement is equal to the min and but the new
						displacement is less, or the current squared difference
						is less than the min square difference.
						*/
						if ((minimumSquaredDifference == -1) || ((minimumSquaredDifference == squaredDifference) && (displacement(dx, dy) < displacement(minimumDx, minimumDy))) || (minimumSquaredDifference > squaredDifference))
						{
							minimumSquaredDifference = squaredDifference;
							minimumDx = dx;
							minimumDy = dy;
						}
					}
				}

				/* 
				Set the value in the depth map. 
				If max displacement is equal to 0, the depth value is just 0.
				*/
				if (minimumSquaredDifference != -1)
				{
					if (maximumDisplacement == 0)
					{
						depth[y * imageWidth + x] = 0;
					}
					else
					{
						depth[y * imageWidth + x] = displacement(minimumDx, minimumDy);
					}
				}
				else
				{
					depth[y * imageWidth + x] = 0;
				}
			}
		}	
}

